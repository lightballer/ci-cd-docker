import { SHOW_EDIT_MODAL, HIDE_EDIT_MODAL, SET_CURRENT_ID, DROP_CURRENT_ID } from './actionTypes';

export const showEditModal = () => ({
  type: SHOW_EDIT_MODAL,
});

export const hideEditModal = () => ({
  type: HIDE_EDIT_MODAL,
});

export const setCurrentId = id => ({
  type: SET_CURRENT_ID,
  payload: {
    id,
  }
});

export const dropCurrentId = () => ({
  type: DROP_CURRENT_ID,
});