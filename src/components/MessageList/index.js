import React from 'react';
import { connect } from 'react-redux';
import './MessageList.css';
import Message from '../Message';
import Divider from '../Divider';
import OwnMessage from '../OwnMessage';
import { editMessage, deleteMessage } from '../Chat/actions';
import { showEditModal, hideEditModal, setCurrentId } from '../EditModal/actions';
import { getTextDateFromISO } from '../../helpers/datesHelper';

class MessageList extends React.Component {

  isDividerRequired(prevDate, curDate) {
    prevDate = prevDate.split('').splice(0, 10).join('');
    curDate = curDate.split('').splice(0, 10).join('');
    if (prevDate === curDate) {
      return false;
    }
    return true;
  }

  deleteMessage(id) {
    this.props.deleteMessage(id);
  }

  showEditModal() {
    this.props.showEditModal();
  }

  hideEditModal() {
    this.props.hideEditModal();
  }

  setCurrentId(id) {
    this.props.setCurrentId(id);
  }
  
  render() {
    let prevDate = '';
    const messages = this.props.messages;
    return (
      <div className="message-list">
        {
          messages.map((message, key) => {
            if (message.user) {
              if (this.isDividerRequired(prevDate, message.createdAt)) {
                prevDate = message.createdAt;
                return (
                  <div key={ message.id } >
                    <Divider date={ getTextDateFromISO(prevDate) }/>
                    <Message 
                      message={ message }
                      showEditModal={ this.showEditModal.bind(this) }
                      setCurrentId={ this.setCurrentId.bind(this) }
                      />
                  </div>
                );
              } else {
                return <Message 
                  message={ message }
                  showEditModal={ this.showEditModal.bind(this) }
                  setCurrentId={ this.setCurrentId.bind(this) }
                  key={ message.id } 
                  />
              }
            } else {
              if (this.isDividerRequired(prevDate, message.createdAt)) {
                prevDate = message.createdAt;
                return (
                  <div className="own-message-container" key={ message.id } >
                    <Divider date={ getTextDateFromISO(prevDate) }/>
                    <OwnMessage
                      message={ message }
                      deleteMessage={ this.deleteMessage.bind(this) }
                      showEditModal={ this.showEditModal.bind(this) }
                      setCurrentId={ this.setCurrentId.bind(this) }/>
                  </div>
                );
              } else {
                return (
                  <div className="own-message-container" key={ message.id } >
                    <OwnMessage 
                      message={ message }
                      deleteMessage={ this.deleteMessage.bind(this) }
                      showEditModal={ this.showEditModal.bind(this) }
                      hideEditModal={ this.hideEditModal.bind(this) }
                      />
                  </div>
                );
              }
            }
          })
        }
      </div> 
    );
  }
}

const mapStateToProps = state => ({
  messages: state.chat.messages,
});

const mapDispathToProps = {
  editMessage,
  deleteMessage,
  showEditModal,
  hideEditModal,
  setCurrentId,
};

export default connect(mapStateToProps, mapDispathToProps)(MessageList);
