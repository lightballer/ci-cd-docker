import React from 'react';
import './Preloader.css';

class Preloader extends React.Component {
  render() {
    return (
      <div className="loader-container">
        <div className="preloader"></div>
      </div>
    )   
  }
}
  
export default Preloader;