const MONTHS = {
	1: 'January',
  2: 'February',
  3: 'March',
  4: 'April',
  5: 'May',
  6: 'June',
  7: 'July',
  8: 'August',
  9: 'September',
  10: 'October',
  11: 'November',
  12: 'December'
};

export function getTimeFromISO(stringISO) {
  const CHARS_IN_TIME = 5;
  stringISO = stringISO.split('');
  return stringISO.splice(stringISO.indexOf('T') + 1, CHARS_IN_TIME).join('');
}

export function getDateAndTimeFromISO(stringISO) {
  stringISO = stringISO.split('T');
  const date = stringISO[0].split('-').reverse().join('.');
  const time = stringISO[1].split('.')[0];
  return `${date} ${time}`;
}

export function getTextDateFromISO(stringISO) {
  stringISO = stringISO.split('T');
  const ddmm = stringISO[0].split('-').reverse().splice(0, 2);
  const day = ddmm[0];
  const monthStr = MONTHS[+ddmm[1]];
  return `${monthStr}, ${day}`;
}

export const getNewId = () => new Date().toISOString();
