import './reset.css';
import './App.css';
import Chat from './components/Chat';
import { URL } from './constants';

function App() {
  return <Chat url={URL}/>;
}

export default App;
