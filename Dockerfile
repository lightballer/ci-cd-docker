FROM node:alpine as builder

WORKDIR /ci-cd-docker

COPY package.json ./

RUN npm install && mkdir /react-homework-2-copy && mv ./node_modules ./react-homework-2-copy

COPY . ./

RUN npm run build

FROM nginx:alpine

COPY --from=builder /ci-cd-docker/build /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]